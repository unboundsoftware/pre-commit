FROM amd64/golang:1.24.1@sha256:f4707a73c390cc4ee035f7527f25907c3e06745ab946628f72cb459b2e70d401

ENV PATH=${PATH}:/root/.local/bin

RUN apt update && \
    apt install -y python3 python3-pip ruby ruby-dev git pipx && \
    rm -rf /var/lib/apt/lists/* && \
    go install github.com/lietu/go-pre-commit@latest && \
    go install golang.org/x/tools/cmd/goimports@latest && \
    pipx install pre-commit
